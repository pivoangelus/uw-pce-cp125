//
//  DummyViewController.m
//  CheckIn
//
//  Created by Justin Andros on 2/23/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "DummyViewController.h"
#import "PhotosCollectionViewController.h"
#import "CheckInManager.h"

@interface DummyViewController ()

@end

@implementation DummyViewController

@end
