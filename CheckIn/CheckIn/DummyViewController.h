//
//  DummyViewController.h
//  CheckIn
//
//  Created by Justin Andros on 2/23/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

//
//  DummyViewController - does nothing but looks pretty!
//

#import <UIKit/UIKit.h>

@interface DummyViewController : UIViewController

@end
