//
//  CheckIn.m
//  CheckIn
//
//  Created by Justin Andros on 2/25/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "CheckIn.h"

@interface CheckIn ()

@property (strong, nonatomic) NSMutableArray *checkInImages;

@end

@implementation CheckIn

static NSString * const coderKey = @"CheckInImages";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _checkInImages = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)images
{
    return [NSArray arrayWithArray:self.checkInImages];
}

- (void)addImage:(UIImage *)image;
{
    [self.checkInImages addObject:image];
}

- (NSInteger)count;
{
    return self.checkInImages.count;
}

#pragma mark - NSSecureCoding

+ (BOOL)supportsSecureCoding;
{
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.checkInImages forKey:coderKey];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self)
    {
        _checkInImages = [coder decodeObjectOfClass:[NSMutableArray class] forKey:coderKey];
    }
    return self;
}

@end
