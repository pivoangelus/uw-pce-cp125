//
//  CheckInManager.h
//  CheckIn
//
//  Created by Justin Andros on 2/25/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

//
//  CheckInManager - Holds CheckIn location objects at each index. Need to change to a dictionary - easier for me to know what
//  location I am trying to load/save.
//

#import <UIKit/UIKit.h>
#import "CheckIn.h"

@interface CheckInManager : NSObject

@property (strong, readonly) NSArray *checkIns;     // readonly to keep people from messing with the array directly
@property (assign, nonatomic) NSInteger count;

- (void)addCheckIn:(CheckIn *)checkIn atIndex:(NSInteger)index;

- (void)save;
- (void)load;

@end
