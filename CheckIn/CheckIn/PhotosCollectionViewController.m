//
//  PhotosCollectionViewController.m
//  CheckIn
//
//  Created by Justin Andros on 2/24/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "PhotosCollectionViewController.h"
#import "PhotosCollectionViewCell.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PhotosCollectionViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) CheckIn *checkIn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *imageLibraryBarButtonItem;

- (IBAction)imageLibraryBarButtonItemTapped:(id)sender;

@end

@implementation PhotosCollectionViewController

static NSString * const reuseIdentifier = @"ImageCellID";

- (void)viewDidLoad
{
    // for now the load just brings in one checkin location, nothing special, nothing good
    [super viewDidLoad];
    
    self.manager = [[CheckInManager alloc] init];
    [self.manager load];
    
    // right now all this does is makes sure it loads in the same checkin location
    // needs to be expanded out to handle multiple checkins.. otherwise, needs to be rewritten badly
    if (self.manager.checkIns.count)
        self.checkIn = self.manager.checkIns[0];
    else
        self.checkIn = [[CheckIn alloc] init];
}

- (IBAction)imageLibraryBarButtonItemTapped:(id)sender;
{
    // ask the device if it has a camera and photo library and then let the user choose
    BOOL isCameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL isPhotoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSAssert([sender isKindOfClass:[UIBarButtonItem class]], @"Source must be a UIBarButtonItem");
    alertController.popoverPresentationController.barButtonItem = sender;
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    if (isCameraAvailable)
    {
        [alertController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self _showImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera source:sender];
        }]];
    }
    
    if (isPhotoLibraryAvailable)
    {
        [alertController addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self _showImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary source:sender];
        }]];
    }
    
    [self showDetailViewController:alertController sender:sender];
}

- (void)_showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType source:(id)sender;
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = sourceType;
    imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    // probably don't need this NSAssert since I checked to confirm earlier it is safe but i like being paranoid for no reason
    NSAssert([sender isKindOfClass:[UIBarButtonItem class]], @"Source must be a UIBarButtonItem");
    imagePicker.popoverPresentationController.barButtonItem = sender;
    imagePicker.delegate = self;
    
    [self showDetailViewController:imagePicker sender:sender];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.checkIn.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.imageView.image = self.checkIn.images[indexPath.item];
    
    return cell;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    // for now add the image, save the checkin location and the manager
    // checkin saving/loading needs to get moved out of here
    [self.checkIn addImage:image];
    [self.manager addCheckIn:self.checkIn atIndex:0];
    [self.manager save];
    
    // trying out the block code to see the animation this time
    [self dismissViewControllerAnimated:YES completion:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.checkIn.count-1 inSection:0];
        [self.collectionView insertItemsAtIndexPaths:@[indexPath]];
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
    }];
}

@end
