//
//  PhotosCollectionViewController.h
//  CheckIn
//
//  Created by Justin Andros on 2/24/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

//
//  PhotosCollectionViewController - need to work on getting a manager from the outside that tells this view
//  what checkin location to load. 
//

#import <UIKit/UIKit.h>
#import "CheckInManager.h"

@interface PhotosCollectionViewController : UICollectionViewController

@property (strong, nonatomic) CheckInManager *manager;

@end
