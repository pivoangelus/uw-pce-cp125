//
//  CheckInManager.m
//  CheckIn
//
//  Created by Justin Andros on 2/25/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "CheckInManager.h"

@interface CheckInManager ()

@property (strong, nonatomic) NSMutableArray *checkInsManager;

@end

@implementation CheckInManager

static NSString * const fileName = @"CheckIn.images";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _checkInsManager = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)checkIns;
{
    return [NSArray arrayWithArray:self.checkInsManager];
}

- (void)addCheckIn:(CheckIn *)checkIn atIndex:(NSInteger)index;
{
    // an ugly way of solving an issue I don't have time to fix -_-
    if ([self.checkInsManager count] > index && index >= 0)
        [self.checkInsManager replaceObjectAtIndex:index withObject:checkIn];
    else
        [self.checkInsManager addObject:checkIn];
}

- (NSInteger)count;
{
    return self.checkInsManager.count;
}

- (void)save;
{
    [NSKeyedArchiver archiveRootObject:self.checkInsManager toFile:self._filePath];
}

- (void)load;
{
    self.checkInsManager = [NSKeyedUnarchiver unarchiveObjectWithFile:self._filePath];
    if (self.checkInsManager == nil)
        self.checkInsManager = [[NSMutableArray alloc] init];
}

- (NSString *)_filePath
{
    // not sure if NSDocumentsDirectory is the best, read some documentation that NSCachesDirectory is better
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dir = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *filePath = [dir stringByAppendingPathComponent:fileName];
    
    return filePath;
}

@end
