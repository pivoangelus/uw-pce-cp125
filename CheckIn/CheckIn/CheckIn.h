//
//  CheckIn.h
//  CheckIn
//
//  Created by Justin Andros on 2/25/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

//
//  CheckIn - stores the images for a checkin location
//

#import <UIKit/UIKit.h>

@interface CheckIn : NSObject <NSSecureCoding>

@property (strong, readonly) NSArray *images;       // readonly to keep people from messing with the array directly
@property (assign, nonatomic) NSInteger count;

- (void)addImage:(UIImage *)image;

@end
