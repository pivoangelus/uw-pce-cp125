//
//  ColorsViewController.m
//  Colors Data
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import "ColorsViewController.h"
#import "RGBViewController.h"

@interface ColorsViewController ()

- (IBAction)redButtonPressed:(id)sender;
- (IBAction)greenButtonPressed:(id)sender;
- (IBAction)blueButtonPressed:(id)sender;

- (void)presentColorView:(ColorsData)color;

@end

@implementation ColorsViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.tabBarItem.image = [UIImage imageNamed:@"colors"];
        self.tabBarItem.title = @"Colors";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (IBAction)redButtonPressed:(id)sender
{
    [self presentColorView:ColorsDataRed];
}

- (IBAction)greenButtonPressed:(id)sender
{
    [self presentColorView:ColorsDataGreen];
}

- (IBAction)blueButtonPressed:(id)sender
{
    [self presentColorView:ColorsDataBlue];
}

- (void)presentColorView:(ColorsData)color
{
    [self.colorsDataManager incrementColor:color];
    RGBViewController *rgbView = [[RGBViewController alloc] init];
    
    rgbView.colorsDataManager = self.colorsDataManager;
    rgbView.color = color;
    
    [self presentViewController:rgbView animated:YES completion:nil];
}

@end
