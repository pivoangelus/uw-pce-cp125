//
//  ColorsDataManager.m
//  Colors Data
//
//  Created by Justin Andros on 1/27/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import "ColorsDataManager.h"

@interface ColorsDataManager ()

@property (assign, nonatomic) NSUInteger red;
@property (assign, nonatomic) NSUInteger green;
@property (assign, nonatomic) NSUInteger blue;

@end

@implementation ColorsDataManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _red = 0;
        _green = 0;
        _blue = 0;
    }
    
    return self;
}

- (void)incrementColor:(ColorsData)color;
{
    switch (color) {
        case ColorsDataRed:
            self.red++;
            break;
        case ColorsDataGreen:
            self.green++;
            break;
        case ColorsDataBlue:
            self.blue++;
            break;
    }
}

- (NSUInteger)countForColor:(ColorsData)color;
{
    NSUInteger count = 0;
    switch (color) {
        case ColorsDataRed:
            count = self.red;
            break;
        case ColorsDataGreen:
            count = self.green;
            break;
        case ColorsDataBlue:
            count = self.blue;
            break;
    }
    return count;
}

- (void)reset
{
    self.red = 0;
    self.green = 0;
    self.blue = 0;
}

@end
