//
//  RGBViewController.m
//  Colors Data
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import "RGBViewController.h"

@interface RGBViewController ()

@property (weak, nonatomic) IBOutlet UILabel *rgbLabel;

- (IBAction)dismiss:(id)sender;

@end

@implementation RGBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    UIColor *currentColor;
    // i wish this could be done in ColorsDataManager :(
    switch (self.color) {
        case ColorsDataRed:
            currentColor = [UIColor redColor];
            break;
        case ColorsDataGreen:
            currentColor = [UIColor greenColor];
            break;
        case ColorsDataBlue:
            currentColor = [UIColor blueColor];
            break;
        default:
            currentColor = [UIColor whiteColor];
            break;
    }
    
    self.view.backgroundColor = currentColor;
    self.rgbLabel.text = [NSString stringWithFormat:@"Presented %lu times.", (unsigned long)[self.colorsDataManager countForColor:self.color]];
}

- (IBAction)dismiss:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
