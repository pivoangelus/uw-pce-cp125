//
//  DataViewController.h
//  Colors Data
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import <UIKit/UIKit.h>
#import "ColorsDataManager.h"

@interface DataViewController : UIViewController

@property (strong, nonatomic) ColorsDataManager *colorsDataManager;

@end
