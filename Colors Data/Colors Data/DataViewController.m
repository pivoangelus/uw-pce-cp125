//
//  DataViewController.m
//  Colors Data
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import "DataViewController.h"

@interface DataViewController ()

@property (weak, nonatomic) IBOutlet UILabel *redDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueDataLabel;

- (IBAction)reset:(id)sender;

@end

@implementation DataViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.tabBarItem.image = [UIImage imageNamed:@"data"];
        self.tabBarItem.title = @"Data";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateLabels];
}

- (void)updateLabels
{
    self.redDataLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[self.colorsDataManager countForColor:ColorsDataRed]];
    self.greenDataLabel.text  = [NSString stringWithFormat:@"%lu", (unsigned long)[self.colorsDataManager countForColor:ColorsDataGreen]];
    self.blueDataLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[self.colorsDataManager countForColor:ColorsDataBlue]];
}

- (IBAction)reset:(id)sender
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:nil
                                                                message:@"Reset presented color views to zero?"
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleCancel
                                         handler:nil]];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"Reset"
                                           style:UIAlertActionStyleDestructive
                                         handler:^(UIAlertAction *action)
                   {
                       [self.colorsDataManager reset];
                       [self updateLabels];
                   }]];
    
    [self presentViewController:ac animated:YES completion:nil];
}

@end
