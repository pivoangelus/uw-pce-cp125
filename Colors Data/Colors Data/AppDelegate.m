//
//  AppDelegate.m
//  Colors Data
//
//  Created by Justin Andros on 1/26/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import "AppDelegate.h"

#import "ColorsViewController.h"
#import "DataViewController.h"

@interface AppDelegate () <UITabBarControllerDelegate>

@property (strong, nonatomic) ColorsDataManager *colorsDataManager;

@end

@implementation AppDelegate

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _colorsDataManager = [[ColorsDataManager alloc] init];
    }
    
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.delegate = self;
    
    ColorsViewController *colorViewController = [[ColorsViewController alloc] init];
    DataViewController *dataViewController = [[DataViewController alloc] init];
    
    colorViewController.colorsDataManager = self.colorsDataManager;
    dataViewController.colorsDataManager = self.colorsDataManager;
    
    tabBarController.viewControllers = @[colorViewController, dataViewController];
    
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
