//
//  ColorsDataManager.h
//  Colors Data
//
//  Created by Justin Andros on 1/27/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
// 

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ColorsData)
{
    ColorsDataRed = 1,
    ColorsDataGreen,
    ColorsDataBlue
};

@interface ColorsDataManager : NSObject

- (void)incrementColor:(ColorsData)color;
- (NSUInteger)countForColor:(ColorsData)color;
- (void)reset;

@end