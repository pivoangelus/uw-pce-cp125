//
//  WeatherDataManager.h
//  Weather
//
//  Created by Justin Andros on 2/3/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger TIME01;
extern NSInteger TIME02;
extern NSInteger TIME03;

@interface WeatherDataManager : NSObject

- (NSString *)cityName;
- (NSString *)sunrise;
- (NSString *)sunset;
- (NSInteger)tempAt:(NSInteger)hour;

@end
