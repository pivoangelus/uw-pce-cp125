//
//  ViewController.m
//  Weather
//
//  Created by Justin Andros on 2/3/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "ViewController.h"
#import "WeatherDataManager.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentTempLabel;
@property (weak, nonatomic) IBOutlet UILabel *time01Label;
@property (weak, nonatomic) IBOutlet UILabel *time02Label;
@property (weak, nonatomic) IBOutlet UILabel *time03Label;
@property (weak, nonatomic) IBOutlet UILabel *temp01Label;
@property (weak, nonatomic) IBOutlet UILabel *temp02Label;
@property (weak, nonatomic) IBOutlet UILabel *temp03Label;
@property (weak, nonatomic) IBOutlet UILabel *sunriseTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunsetTimeLabel;

@property (strong, nonatomic) WeatherDataManager *wdm;

@end

@implementation ViewController

- (void)awakeFromNib;
{
    self.wdm = [[WeatherDataManager alloc] init];
}

- (void)viewWillAppear:(BOOL)animated;
{
    [super viewWillAppear:animated];
    
    self.cityNameLabel.text = self.wdm.cityName;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger hour = [calendar component:NSCalendarUnitHour fromDate:[NSDate date]];
    
    self.currentTempLabel.text = [NSString stringWithFormat:@"%ld", (long)[self.wdm tempAt:hour]];
    
    self.time01Label.text = [self formattedHour:TIME01];
    self.temp01Label.text = [NSString stringWithFormat:@"%ld℉", (long)[self.wdm tempAt:TIME01]];
    
    self.time02Label.text = [self formattedHour:TIME02];
    self.temp02Label.text = [NSString stringWithFormat:@"%ld℉", (long)[self.wdm tempAt:TIME02]];
    
    self.time03Label.text = [self formattedHour:TIME03];
    self.temp03Label.text = [NSString stringWithFormat:@"%ld℉", (long)[self.wdm tempAt:TIME03]];
    
    self.sunriseTimeLabel.text = self.wdm.sunrise;
    self.sunsetTimeLabel.text = self.wdm.sunset;
}

- (NSString *)formattedHour:(NSInteger)hour
{
    // quite possibly a better way to do this but for now...
    NSString *standardTime;
    
    switch (hour)
    {
        case 0:
            standardTime = @"12 AM";
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
            standardTime = [NSString stringWithFormat:@"%ld AM", (long)hour];
            break;
        case 12:
            standardTime = @"12 PM";
            break;
        case 13:
        case 14:
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
            hour -= 12;
            standardTime = [NSString stringWithFormat:@"%ld PM", (long)hour];
            break;
    }

    return standardTime;
}

@end
