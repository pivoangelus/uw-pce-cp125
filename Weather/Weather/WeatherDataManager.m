//
//  WeatherDataManager.m
//  Weather
//
//  Created by Justin Andros on 2/3/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "WeatherDataManager.h"

// military times only; only needed three numbers so i am keeping it simple
NSInteger TIME01 = 8;
NSInteger TIME02 = 9;
NSInteger TIME03 = 10;

@interface WeatherDataManager()

@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSDictionary *weatherDictionary;

@end

@implementation WeatherDataManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSURL *weatherDataURL = [[NSBundle mainBundle] URLForResource:@"WeatherData" withExtension:@"plist"];
        self.data = [NSArray arrayWithContentsOfURL:weatherDataURL];
        self.weatherDictionary = [self.data objectAtIndex:0];
    }
    return self;
}

- (NSString *)cityName;
{
    return [self.weatherDictionary objectForKey:@"City"];
}

- (NSString *)sunrise;
{
    return [self.weatherDictionary objectForKey:@"Sunrise"];
}

- (NSString *)sunset;
{
    return [self.weatherDictionary objectForKey:@"Sunset"];
}

- (NSInteger)tempAt:(NSInteger)hour;
{
    NSArray *hourlyForecast = (NSArray *)[self.weatherDictionary objectForKey:@"HourlyForecast"];
    NSNumber *temp = [hourlyForecast objectAtIndex:hour];
    
    return [temp integerValue];
}

@end
