//
//  PlaceAnnotation.h
//  CheckedIn
//
//  Created by Justin Andros on 3/2/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

// user searched locations

#import <MapKit/MapKit.h>

@interface PlaceAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;

@end
