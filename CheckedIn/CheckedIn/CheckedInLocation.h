//
//  CheckedLocation.h
//  CheckedIn
//
//  Created by Justin Andros on 3/2/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckedInLocation : NSObject

@property (nonatomic, strong) NSString *title;
@property (readonly, strong) NSArray *images;

- (void)addImage:(UIImage *)image;

@end
