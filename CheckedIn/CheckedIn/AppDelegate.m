//
//  AppDelegate.m
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@end

NSString * const SaveImageSettingsKey = @"SaveImageSettingsKey";

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{ SaveImageSettingsKey : @YES }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:NSUserDefaultsDidChangeNotification
                                                      object:[NSUserDefaults standardUserDefaults]
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      [[NSUserDefaults standardUserDefaults] synchronize];
                                                  }];
    
    return YES;
}

- (void)dealloc
{
    // make sure to remove observer. appdelegate will take care of this but it is nice to have.
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] removeObserver:[NSUserDefaults standardUserDefaults]];
}

@end
