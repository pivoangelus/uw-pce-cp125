//
//  PhotosCollectionViewController.m
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

// displays images/photos selected by the user for a location.

#import "PhotosCollectionViewController.h"
#import "PhotosCollectionViewCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h"

@interface PhotosCollectionViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) CheckedInLocation *checkedInLocation;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *imageLibraryBarButton;

- (IBAction)imageLibraryBarButtonItemTapped:(id)sender;

@end

@implementation PhotosCollectionViewController

static NSString * const reuseIdentifier = @"PhotosCellID";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.checkedInLocation = self.manager.checkedIn[self.managerIndex];
}

- (IBAction)imageLibraryBarButtonItemTapped:(id)sender;
{
    // ask the device if it has a camera and photo library and then let the user choose
    BOOL isCameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL isPhotoLibraryAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSAssert([sender isKindOfClass:[UIBarButtonItem class]], @"Source must be a UIBarButtonItem");
    alertController.popoverPresentationController.barButtonItem = sender;
    
    // l10n
    NSString *alertCancel = NSLocalizedString(@"photos-collection-view-controller.alert-cancel", @"end request"); //@"Cancel"
    [alertController addAction:[UIAlertAction actionWithTitle:alertCancel style:UIAlertActionStyleCancel handler:nil]];
    
    if (isCameraAvailable)
    {
        // l10n
        NSString *alertCamera = NSLocalizedString(@"photos-collection-view.alert-camera", @"photographic equipment"); //@"Camera";
        [alertController addAction:[UIAlertAction actionWithTitle:alertCamera style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self _showImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera source:sender];
        }]];
    }
    
    if (isPhotoLibraryAvailable)
    {
        // l10n
        NSString *alertPhotoLibrary = NSLocalizedString(@"photos-collection-view-controller.alert-photo-library", @"collection of photos"); //@"Photo Library"
        [alertController addAction:[UIAlertAction actionWithTitle:alertPhotoLibrary style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self _showImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary source:sender];
        }]];
    }
    
    [self showDetailViewController:alertController sender:sender];
}

- (void)_showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType source:(id)sender;
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = sourceType;
    imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    // probably don't need this NSAssert since I checked to confirm earlier it is safe but i like being paranoid for no reason
    NSAssert([sender isKindOfClass:[UIBarButtonItem class]], @"Source must be a UIBarButtonItem");
    imagePicker.popoverPresentationController.barButtonItem = sender;
    imagePicker.delegate = self;
    
    [self showDetailViewController:imagePicker sender:sender];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.checkedInLocation.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.imageView.image = self.checkedInLocation.images[indexPath.item];
    
    return cell;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    BOOL canSave = [[NSUserDefaults standardUserDefaults] boolForKey:SaveImageSettingsKey];
    BOOL isCameraImage = (picker.sourceType == UIImagePickerControllerSourceTypeCamera) ? YES : NO;
        
    if (canSave && isCameraImage)
    {
        UIImageWriteToSavedPhotosAlbum(image, nil, NULL, NULL);
    }
    
    [self.checkedInLocation addImage:image];
    [self.manager save];
    
    // trying out the block code to see the animation this time
    [self dismissViewControllerAnimated:YES completion:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.checkedInLocation.images.count-1 inSection:0];
        [self.collectionView insertItemsAtIndexPaths:@[indexPath]];
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
    }];
}

@end
