//
//  PhotosCollectionViewController.h
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

// collection of images 'related' to the location

#import <UIKit/UIKit.h>
#import "CheckedInManager.h"

@interface PhotosCollectionViewController : UICollectionViewController

@property (nonatomic, strong) CheckedInManager *manager;
@property (nonatomic, assign) NSInteger managerIndex;   // what the user has chosen from table view

@end
