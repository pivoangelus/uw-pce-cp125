//
//  PhotosCollectionViewCell.h
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
