//
//  MapViewController.m
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "MapViewController.h"
#import "PlaceAnnotation.h"
#import "CheckInTableViewController.h"
#import <MapKit/MapKit.h>

@interface MapViewController () <CLLocationManagerDelegate, MKMapViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, strong) CheckInTableViewController *checkIntableViewController;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) MKLocalSearch *localSearch;

- (IBAction)cancelTapped:(id)sender;

@end

@implementation MapViewController

- (void)viewDidLoad;
{
    [super viewDidLoad];
    
    if (self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.delegate = self;
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)dismissMapView;
{
    // remove annotations when dismissing the view. user shouldn't need these reloaded.
    NSArray *placemarksToRemove = [self.mapView.annotations filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        BOOL isUserLocation = [evaluatedObject isKindOfClass:[MKUserLocation class]];
        BOOL shouldKeepInArray = (isUserLocation == NO);
        
        return shouldKeepInArray;
    }]];
    [self.mapView removeAnnotations:placemarksToRemove];
    
    // remove ourselves from the stack. don't want the user to be able to come back without pushing add (+)
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelTapped:(id)sender
{
    [self dismissMapView]; 
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        self.mapView.showsUserLocation = YES;
    }
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation;
{
    [mapView setRegion:MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 30, 30) animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation;
{
    if ([annotation isKindOfClass:[PlaceAnnotation class]])
    {
        MKPinAnnotationView *viewPin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"viewPin"];
        
        if (viewPin == nil)
        {
            viewPin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"viewPin"];
            viewPin.pinColor = MKPinAnnotationColorRed;
            viewPin.animatesDrop = YES;
            viewPin.canShowCallout = YES;
            
            // 'custom' title button
            UIButton *calloutButton = [UIButton buttonWithType:UIButtonTypeSystem];
            
            // l10n
            NSString *calloutTitle = NSLocalizedString(@"map-view-controller.callout-title", @"sign in to a location"); // @"Check In"
            [calloutButton setTitle:calloutTitle forState:UIControlStateNormal];
            // target handled by mapView:annotationView:calloutAccessoryControlTapped:
            [calloutButton addTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
            [calloutButton sizeToFit];
            
            viewPin.rightCalloutAccessoryView = calloutButton;
        }
        else
        {
            viewPin.annotation = annotation;
        }
        return viewPin; // search request pin
    }
    
    return nil; // default
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control;
{
    // callout button has been tapped
    // set the title, add to the manager, save and move back to root
    CheckedInLocation *location = [[CheckedInLocation alloc] init];
    location.title = view.annotation.title;
    [self.manager addCheckedInObject:location];
    [self.manager save];    // save new location
    [self dismissMapView];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    if (self.localSearch.isSearching)
    {
        [self.localSearch cancel];
    }
    
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    searchRequest.region = self.mapView.region;
    searchRequest.naturalLanguageQuery = searchBar.text;
    
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (error != nil)
        {
            NSString *errorStr = [[error userInfo] valueForKey:NSLocalizedDescriptionKey];
            
            // l10n
            NSString *alertTitle = NSLocalizedString(@"map-view-controller.alert-title", @"unable to locate request"); //@"Could not find places";
            NSString *alertOk = NSLocalizedString(@"map-view-controller.alert-ok", @"approve"); //@"OK";
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle
                                                                           message:errorStr
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *alertOK = [UIAlertAction actionWithTitle:alertOk style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:alertOK];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            for (MKMapItem *item in [response mapItems])
            {
                PlaceAnnotation *annotation = [[PlaceAnnotation alloc] init];
                annotation.coordinate = item.placemark.location.coordinate;
                annotation.title = item.name;
                [self.mapView addAnnotation:annotation];
            }
        }
    }];
    
    if (self.localSearch != nil)
    {
        self.localSearch = nil;
    }
    
    searchBar.text = nil; // search is finished, clear search bar for another request
}
@end
