//
//  CheckedInManager.m
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "CheckedInManager.h"

@interface CheckedInManager ()

@property (nonatomic, strong) NSMutableArray *checkedInLocations;
@end

@implementation CheckedInManager

static NSString * const fileName = @"CheckedIn.bin";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _checkedInLocations = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)checkedIn;
{
    return [NSArray arrayWithArray:self.checkedInLocations];
}

- (void)addCheckedInObject:(CheckedInLocation *)location;
{
    [self.checkedInLocations addObject:location];
}

- (void)save;
{
    [NSKeyedArchiver archiveRootObject:self.checkedInLocations toFile:self._filePath];
}

- (void)load;
{
    self.checkedInLocations = [NSKeyedUnarchiver unarchiveObjectWithFile:self._filePath];
    if (self.checkedInLocations == nil)
        self.checkedInLocations = [[NSMutableArray alloc] init];
}

- (NSString *)_filePath;
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dir = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *filePath = [dir stringByAppendingPathComponent:fileName];
    
    return filePath;
}

@end
