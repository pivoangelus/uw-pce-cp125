//
//  MapViewController.h
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckedInManager.h"

@interface MapViewController : UIViewController

@property (nonatomic, strong) CheckedInManager *manager;

@end
