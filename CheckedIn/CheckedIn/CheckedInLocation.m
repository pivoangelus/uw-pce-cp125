//
//  CheckedLocation.m
//  CheckedIn
//
//  Created by Justin Andros on 3/2/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "CheckedInLocation.h"

@interface CheckedInLocation ()

@property (nonatomic, strong) NSMutableArray *checkedInImages;

@end

@implementation CheckedInLocation

static NSString * const titleKey = @"titleKey";
static NSString * const imagesKey = @"imagesKey";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _checkedInImages = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)images;
{
    return [NSArray arrayWithArray:self.checkedInImages];
}

- (void)addImage:(UIImage *)image;
{
    [self.checkedInImages addObject:image];
}

#pragma mark - NSSecureCoding

+ (BOOL)supportsSecureCoding;
{
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:self.title forKey:titleKey];
    [coder encodeObject:self.checkedInImages forKey:imagesKey];
}

- (instancetype)initWithCoder:(NSCoder *)coder;
{
    self = [super init];
    if (self)
    {
        _title = [coder decodeObjectOfClass:[NSString class] forKey:titleKey];
        _checkedInImages = [coder decodeObjectOfClass:[NSMutableArray class] forKey:imagesKey];
    }
    return self;
}

@end
