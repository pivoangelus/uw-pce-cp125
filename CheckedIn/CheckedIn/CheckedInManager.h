//
//  CheckedInManager.h
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckedInLocation.h"

@interface CheckedInManager : NSObject

@property (readonly, strong) NSArray *checkedIn;

- (void)addCheckedInObject:(CheckedInLocation *)location;
- (void)save;
- (void)load;

@end
