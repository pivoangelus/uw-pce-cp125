//
//  CheckInTableViewController.m
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "CheckInTableViewController.h"
#import "PhotosCollectionViewController.h"
#import "MapViewController.h"

#pragma mark - PushSegue

// PushSegue only used here. Seems easier to understand what I am doing. Still like making the segues via code vs IB

@interface PushSegue : UIStoryboardSegue
@end

@implementation PushSegue

- (void)performSegueFromSource:(UIViewController *)source toDestination:(UIViewController *)destination;
{
    source = self.sourceViewController;
    destination = self.destinationViewController;
    [source.navigationController pushViewController:destination animated:YES];
}

@end

#pragma mark - CheckInTableViewController

@interface CheckInTableViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) PushSegue *mapSegue;
@property (nonatomic, strong) PushSegue *photosSegue;
@property (nonatomic, strong) PhotosCollectionViewController *photosCollectionViewController;
@property (nonatomic, strong) MapViewController *mapViewController;

@property (nonatomic, strong) CheckedInLocation *checkedInLocation;

- (IBAction)addLocationTapped:(id)sender;

@end

@implementation CheckInTableViewController

- (void)awakeFromNib
{
    self.manager = [[CheckedInManager alloc] init];
    [self.manager load];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // reuse the same map view
    self.mapViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"MapViewControllerID"];
    
    // segue connected
    self.mapSegue = [[PushSegue alloc] initWithIdentifier:@"showMap"
                                                   source:self
                                              destination:self.mapViewController];
}

- (IBAction)addLocationTapped:(id)sender
{
    self.mapViewController.manager = self.manager;

    [self.mapSegue performSegueFromSource:self toDestination:self.mapViewController];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.manager.checkedIn.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // don't need to use a custom class for cells
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CheckInCellID" forIndexPath:indexPath];
    
    self.checkedInLocation = [[CheckedInLocation alloc] init];
    self.checkedInLocation = self.manager.checkedIn[indexPath.row];
    cell.textLabel.text = self.checkedInLocation.title;
        
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // create a new collection view, send the working manager and the index chosen by the user
    self.photosCollectionViewController = [[self storyboard] instantiateViewControllerWithIdentifier:@"PhotosCollectionViewControllerID"];
    self.photosCollectionViewController.manager = self.manager;
    self.photosCollectionViewController.managerIndex = indexPath.row;

    self.photosSegue = [[PushSegue alloc] initWithIdentifier:@"showCollection"
                                                      source:self
                                                 destination:self.photosCollectionViewController];
    [self.photosSegue performSegueFromSource:self toDestination:self.photosCollectionViewController];
}

@end
