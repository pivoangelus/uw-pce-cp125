//
//  CheckInTableViewController.h
//  CheckedIn
//
//  Created by Justin Andros on 3/1/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

// CheckInTableViewController -- displays all the locations 'visited' and connects to the map
// view and collection view of photos

#import <UIKit/UIKit.h>
#import "CheckedInManager.h"


@interface CheckInTableViewController : UITableViewController

@property (nonatomic, strong) CheckedInManager *manager;

@end
