//
//  BirthdaysDataManager.m
//  Birthdays
//
//  Created by Justin Andros on 2/9/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

//
// an array of birthdays with at each index a dictionary for three items: name, birthday, and days until next birthday
// not sure why i chose this appoarch versus just an array, but it seemed to organise itself better
//

#import "BirthdaysDataManager.h"
#import "NSDate+UWDateMath.h"

@interface BirthdaysDataManager ()

@property (strong, nonatomic) NSMutableArray *birthdayArray;

@end

@implementation BirthdaysDataManager

// keys for the dictionary found in the array of each index
static NSString * nameKey      = @"Name";
static NSString * dateKey      = @"Date";
static NSString * daysUntilKey = @"DaysUntil";

- (instancetype)initWithData
{
    self = [super init];
    if (self)
    {
        _birthdayArray = [[NSMutableArray alloc] init];
    }

    // much nicer way to get a date then using nscalendar and components -_-
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    
    NSDate *august29th1981 = [formatter dateFromString:@"08/29/1981"];
    NSDate *december3rd1984 = [formatter dateFromString:@"12/03/1984"];

    [self insertName:@"Justin" andDate:august29th1981];
    [self insertName:@"Dalin" andDate:december3rd1984];
    
    return self;
}

- (void)insertName:(NSString *)name andDate:(NSDate *)date;
{
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:name, nameKey, date, dateKey, [self daysUntilNextBirthday:date], daysUntilKey, nil];
    
    [self.birthdayArray addObject:dictionary];
    [self sortArray];
}

- (void)sortArray
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"DaysUntil.integerValue" ascending:YES];
    [self.birthdayArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
}

- (NSInteger)birthdayArrayCount;
{
    return [self.birthdayArray count];
}

- (NSString *)getNameAtIndex:(NSInteger)index;
{
    return self.birthdayArray[index][nameKey];
}

- (NSString *)getFormattedDateAtIndex:(NSInteger)index;
{
    // updated to static formatter per class
    // NSDateFormatter is expensive to alloc/init
    static NSDateFormatter *longDateFormatter = nil;
    static dispatch_once_t longDateFormatterOnceToken;
    
    dispatch_once(&longDateFormatterOnceToken, ^{
        longDateFormatter = [[NSDateFormatter alloc] init];
        [longDateFormatter setDateStyle:NSDateFormatterLongStyle];
    });
    
    return [longDateFormatter stringFromDate:self.birthdayArray[index][dateKey]];
}

- (NSString *)getDaysUntilNextAtIndex:(NSInteger)index;
{
    return [NSString stringWithFormat:@"%@", self.birthdayArray[index][daysUntilKey]];;
}

- (NSNumber *)daysUntilNextBirthday:(NSDate *)date;
{
    NSInteger daysUntilInteger = [date daysUntilNextOccurrence];
    return [NSNumber numberWithInteger:daysUntilInteger];
}

@end
