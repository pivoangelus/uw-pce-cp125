//
//  BirthdayTableViewCell.h
//  Birthdays
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BirthdayTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *daysUntilNextLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;

@end
