//
//  BirthdayTableViewController.m
//  Birthdays
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "BirthdayTableViewController.h"
#import "BirthdayTableViewCell.h"
#import "AddTableViewController.h"
#import "BirthdaysDataManager.h"

@interface BirthdayTableViewController () <AddTableViewControllerDelegate>

@property (strong, nonatomic) BirthdaysDataManager *dataManager;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BirthdayTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 80;
    
    self.dataManager = [[BirthdaysDataManager alloc] initWithData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
{
    NSAssert([segue.destinationViewController isKindOfClass:[UINavigationController class]], @"destination controller expected to be of type UINavigationController");
    
    UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
    
    NSAssert([navController.topViewController isKindOfClass:[AddTableViewController class]], @"topViewController expected to be of type NameInputViewController");
    
    AddTableViewController *nameInputViewController = (AddTableViewController *)navController.topViewController;
    nameInputViewController.delegate = self;
}


#pragma mark - TableView setup

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataManager birthdayArrayCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BirthdayTableViewCell *cell = (BirthdayTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BirthdayCellID" forIndexPath:indexPath];
    
    cell.nameLabel.text = [self.dataManager getNameAtIndex:indexPath.row];
    cell.birthdayLabel.text = [self.dataManager getFormattedDateAtIndex:indexPath.row];
    cell.daysUntilNextLabel.text = [self.dataManager getDaysUntilNextAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - AddTableViewControllerDelegate

- (void)addTableViewController:(AddTableViewController *)addTableViewController didEnterName:(NSString *)name andDate:(NSDate *)date;
{
    // TODO: update this later with what was demostrated in class/play with the differences
    [self.tableView reloadData];
    [self.dataManager insertName:name andDate:date];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
