//
//  BirthdaysDataManager.h
//  Birthdays
//
//  Created by Justin Andros on 2/9/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BirthdaysDataManager : NSObject

- (instancetype)initWithData;
- (void)insertName:(NSString *)name andDate:(NSDate *)date;
- (NSInteger)birthdayArrayCount;
- (NSString *)getNameAtIndex:(NSInteger)index;
- (NSString *)getFormattedDateAtIndex:(NSInteger)index;
- (NSString *)getDaysUntilNextAtIndex:(NSInteger)index;

@end
