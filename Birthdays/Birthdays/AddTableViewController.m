//
//  AddTableViewController.m
//  Birthdays
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import "AddTableViewController.h"

@interface AddTableViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *chosenBirthdayLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *birthdayDatePicker;

- (IBAction)doneButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;

@end

@implementation AddTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nameTextField.delegate = self;
    
    [self.birthdayDatePicker addTarget:self action:@selector(updateBirthdayLabel) forControlEvents:UIControlEventValueChanged];
    [self updateBirthdayLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (IBAction)doneButtonTapped:(id)sender;
{
    // not the best way to stop blank names but i figure if you put spaces why not give it to them for this
    if ([self.nameTextField.text isEqual:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter a name."
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self.delegate addTableViewController:self didEnterName:self.nameTextField.text andDate:self.birthdayDatePicker.date];
    }
}

- (IBAction)cancelButtonTapped:(id)sender;
{
    if ([self.delegate respondsToSelector:@selector(addTableViewControllerDidCancel:)])
        [self.delegate addTableViewControllerDidCancel:self];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Updates

- (void)updateBirthdayLabel;
{
    // updated to static formatter per class
    // NSDateFormatter is expensive to alloc/init
    static NSDateFormatter *longDateFormatter = nil;
    static dispatch_once_t longDateFormatterOnceToken;
    
    dispatch_once(&longDateFormatterOnceToken, ^{
        longDateFormatter = [[NSDateFormatter alloc] init];
        [longDateFormatter setDateStyle:NSDateFormatterLongStyle];
    });
        
    self.chosenBirthdayLabel.text = [longDateFormatter stringFromDate:self.birthdayDatePicker.date];
}

#pragma mark - TableView setup

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

// call both, tableView:estimatedHeightForRowAtIndexPath: and tableView:heightForRowAtIndexPath: to avoid warnings
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewIndex:indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableViewIndex:indexPath.row];
}

- (CGFloat)tableViewIndex:(NSUInteger)index
{
    CGFloat height = 0.0;
    switch (index) {
        case 0:
        case 1:
            height = 44;;
            break;
        case 2:
            height = 200;
            break;
    }
    return height;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
