//
//  AddTableViewController.h
//  Birthdays
//
//  Created by Justin Andros on 2/8/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddTableViewControllerDelegate;

@interface AddTableViewController : UITableViewController

@property (weak, nonatomic) id<AddTableViewControllerDelegate> delegate;

@end

@protocol AddTableViewControllerDelegate <NSObject>

- (void)addTableViewController:(AddTableViewController *)addTableViewController didEnterName:(NSString *)name andDate:(NSDate *)date;

// wanted to use an @optional for later reference. plus, cancel just seems to naturally put itself as an @optional method
@optional
- (void)addTableViewControllerDidCancel:(AddTableViewController *)addTableViewController;

@end